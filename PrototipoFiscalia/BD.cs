﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototipoFiscalia
{
    class Individuo
    {
        public string Rut { get; set; }
        public string FechaNacimiento { get; set; }
        public List<string> Delitos { get; set; }
        public List<string> Rucs { get; set; }
        public List<string> Relaciones { get; set; }
    }


    class Evento {
        public string Ruc { get; set; }
        public List<string> Individuos { get; set; }
    }


    class Relacion
    {
        public string Id { get; set; }
        public List<string> Individuos { get; set; }
        public int CantidadRelaciones { get; set; }
    }
}
