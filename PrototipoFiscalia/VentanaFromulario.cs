﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Raven.Client;
using Raven.Client.Documents;
using Raven.Embedded;
using Raven.Client.ServerWide.Operations;

namespace PrototipoFiscalia
{
    public partial class VentanaFromulario : Form
    {
        public VentanaFromulario()
        {
            InitializeComponent();
        }


        private void btnBuscarArchivoEntrada_Click(object sender, EventArgs e)
        {
            if (dialogArchivoEntrada.ShowDialog() == DialogResult.OK)
            {
                lblNombreArchivoEntrada.Text = dialogArchivoEntrada.SafeFileName;
                dialogSeleccionarDirectorioSalida.SelectedPath = Path.GetDirectoryName(dialogArchivoEntrada.FileName);
                lblDirectorioSalida.Text = Path.GetDirectoryName(dialogArchivoEntrada.FileName);
            }
        }


        private void btnSiguienteEtapaEntrada_Click(object sender, EventArgs e)
        {
            if (dialogArchivoEntrada.FileName == "")
            {
                btnBuscarArchivoEntrada.Focus();
                MessageBox.Show("Debe seleccionar un archivo", "Seleccionar archivo...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (rbIndividuoIndividuoRelacion.Checked || rbMatrizEventos.Checked)
                    cbIndicadores.Enabled = false;
                else
                    cbIndicadores.Enabled = true;
                panelEtapaSalida.Visible = true;
                panelEtapaEntrada.Visible = false;
            }
        }


        private void btnAnteriorEtapaSalida_Click(object sender, EventArgs e)
        {
            panelEtapaEntrada.Visible = true;
            panelEtapaSalida.Visible = false;
        }


        private void cbMatrizSncse_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMatrizSncse.Checked)
                tbSigma.Enabled = true;
            else
            {
                tbSigma.Enabled = false;
                tbSigma.Text = "";
            }
        }


        private void tbSigma_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            TextBox thisBox = (TextBox)sender;
            if ((e.KeyChar == '.') && (thisBox.Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }


        private void tbSigma_TextChanged(object sender, EventArgs e)
        {
            String sigma = tbSigma.Text;

            if (sigma.Length > 0)
            {
                String punto = ".";
                if (String.Equals(sigma.Substring(0, 1), punto))
                {
                    tbSigma.Text = "0.";
                }
                else
                {
                    int valorInicial = Int32.Parse(sigma.Substring(0, 1));
                    if (valorInicial != 0)
                    {
                        tbSigma.Text = String.Concat("0.", sigma.Substring(0, 1));
                    }
                    if (tbSigma.Text.Length > 1 && !String.Equals(tbSigma.Text.Substring(1, 1), punto))
                    {
                        tbSigma.Text = String.Concat("0.", tbSigma.Text.Substring(1, 1));
                    }
                }
            }
            tbSigma.SelectionStart = tbSigma.Text.Length;
        }


        private void btnSeleccionDirectorioSalida_Click(object sender, EventArgs e)
        {
            if (dialogSeleccionarDirectorioSalida.ShowDialog() == DialogResult.OK)
                lblDirectorioSalida.Text = dialogSeleccionarDirectorioSalida.SelectedPath;
        }


        private void btnSiguienteEtapaSalida_Click(object sender, EventArgs e)
        {
            bool etapaValida = true;

            if (!cbIndividuoIndividuoRelacion.Checked && !cbMatrizEventos.Checked && !cbMatrizDistancia.Checked
                && !cbMatrizVinculo.Checked && !cbMatrizSncse.Checked && !cbIndicadores.Checked)
            {
                etapaValida = false;
                MessageBox.Show("Debe seleccionar al menos un archivo de salida", "Seleccionar...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (cbMatrizSncse.Checked && tbSigma.Text == "")
                {
                    etapaValida = false;
                    MessageBox.Show("Si selecciona la Matriz SNCSE debe asignar un valor a Sigma.", "Sigma Vacío", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbSigma.Focus();
                }
            }

            if (etapaValida)
            {
                panelEtapaResultados.Visible = true;
                panelEtapaSalida.Visible = false;
                rtbLogResultados.Text = "Iniciando..." + Environment.NewLine;
                bgwOperaciones.RunWorkerAsync();
            }
        }


        private void btnNuevoEtapaResultado_Click(object sender, EventArgs e)
        {
            // Setear a valores por defecto en Etapa Entrada
            rbIndividuoEvento.Checked = true;
            rbIndividuoEventoFechaFecha.Checked = false;
            rbIndividuoIndividuoRelacion.Checked = false;
            rbMatrizEventos.Checked = false;
            rbPuntoComa.Checked = true;
            rbComa.Checked = false;
            rbEspacio.Checked = false;
            dialogArchivoEntrada.Reset();
            lblNombreArchivoEntrada.Text = "";

            // Setear a valores por defecto en Etapa Salida
            cbIndividuoIndividuoRelacion.Checked = false;
            cbMatrizEventos.Checked = false;
            cbMatrizDistancia.Checked = false;
            cbMatrizVinculo.Checked = false;
            cbMatrizSncse.Checked = false;
            cbIndicadores.Checked = false;
            tbSigma.Enabled = false;
            tbSigma.Text = "";
            dialogSeleccionarDirectorioSalida.Reset();
            lblDirectorioSalida.Text = "";

            // Setear a valores por defecto en Etapa de Resultados
            rtbLogResultados.Text = "";

            // Cambiar visibilidad de paneles contenedores de etapas
            panelEtapaEntrada.Visible = true;
            panelEtapaSalida.Visible = false;
            panelEtapaResultados.Visible = false;
        }
        

        private void btnVerResultados_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(dialogSeleccionarDirectorioSalida.SelectedPath);
        }


        private void btnFinalizarEtapaResultado_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void bgwOperaciones_DoWork(object sender, DoWorkEventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;

            // Inicializar BD
            EmbeddedServer.Instance.StartServer();
            using (var bd = EmbeddedServer.Instance.GetDocumentStore("BdPrototipoFiscalia"))
            {
                //EmbeddedServer.Instance.OpenStudioInBrowser();
                // Leer archivo
                rtbLogResultados.SelectionStart = rtbLogResultados.Text.Length;
                rtbLogResultados.SelectionColor = Color.Black;
                rtbLogResultados.SelectedText = "Leyendo archivo " + dialogArchivoEntrada.FileName;
                int cantidadLineas = File.ReadLines(this.dialogArchivoEntrada.FileName).Count();
                StreamReader archivo = new StreamReader(this.dialogArchivoEntrada.FileName);
                string linea = "";
                int numeroLineas = 0;
                char separador = ';';
                if (rbComa.Checked)
                    separador = ',';
                if (rbEspacio.Checked)
                    separador = ' ';
                if (rbPuntoComa.Checked)
                    separador = ';';
                lblTituloProgreso.Text = "Leyendo archivo...";
                while ((linea = archivo.ReadLine()) != null)
                {
                    using (var sesionBd = bd.OpenSession())
                    {
                        sesionBd.Advanced.WaitForIndexesAfterSaveChanges();
                        numeroLineas++;
                        int porcentaje = (numeroLineas * 100) / cantidadLineas;
                        int maxRequest = sesionBd.Advanced.MaxNumberOfRequestsPerSession;
                        sesionBd.Advanced.MaxNumberOfRequestsPerSession = maxRequest + 30;
                        string[] campos = linea.Split(separador);
                        string rut = null, ruc = null, fechaNacimiento = null, fechaDelito = null;
                        if (rbIndividuoEvento.Checked && campos.Length < 2)
                        {
                            CambiarProgreso(porcentaje);
                            continue;
                        }
                        else if (rbIndividuoEvento.Checked && campos.Length >= 2)
                        {
                            rut = campos[0].ToUpper();
                            ruc = campos[1].ToUpper();
                        }

                        if (rbIndividuoEventoFechaFecha.Checked && campos.Length < 4)
                        {
                            CambiarProgreso(porcentaje);
                            continue;
                        }
                        else if (rbIndividuoEventoFechaFecha.Checked && campos.Length >= 4)
                        {
                            rut = campos[0].ToUpper();
                            ruc = campos[1].ToUpper();
                            fechaNacimiento = campos[2].ToUpper();
                            fechaDelito = campos[3].ToUpper();
                        }

                        // Tabla individuo
                        List<Individuo> listaIndividuos = sesionBd.Query<Individuo>().Where(x => x.Rut == rut).ToList();
                        Individuo individuo;
                        if (!listaIndividuos.Any())
                        {
                            individuo = new Individuo();
                            individuo.Rut = rut;
                            individuo.Rucs = new List<string>();
                            individuo.Delitos = new List<string>();
                            individuo.Relaciones = new List<string>();
                            individuo.Rucs.Add(ruc);
                            if (rbIndividuoEventoFechaFecha.Checked)
                            {
                                individuo.FechaNacimiento = fechaNacimiento;
                                individuo.Delitos.Add(fechaDelito);
                            }
                            sesionBd.Store(individuo);
                        }
                        else
                        {
                            individuo = listaIndividuos.First();
                            if (!individuo.Rucs.Contains(ruc))
                                individuo.Rucs.Add(ruc);
                            if (rbIndividuoEventoFechaFecha.Checked)
                                individuo.Delitos.Add(fechaDelito);
                        }

                        // Tabla evento
                        List<Evento> listaEventos = sesionBd.Query<Evento>().Where(x => x.Ruc == ruc).ToList();
                        Evento evento;
                        if (!listaEventos.Any())
                        {
                            evento = new Evento();
                            evento.Individuos = new List<string>();
                            evento.Ruc = ruc;
                            evento.Individuos.Add(rut);
                            sesionBd.Store(evento);
                        }
                        else
                        {
                            evento = listaEventos.First();
                            if (!evento.Individuos.Contains(rut))
                                evento.Individuos.Add(rut);
                        }

                        // Guardar las dos tablas que son independientes
                        sesionBd.SaveChanges();

                        // Tabla Relacion
                        foreach (string companiero in evento.Individuos)
                        {
                            if (companiero != rut)
                            {
                                List<Individuo> listaCompanieroBd = sesionBd.Query<Individuo>().Where(x => x.Rut == companiero).ToList();
                                if (listaCompanieroBd.Any())
                                {
                                    Individuo companieroBd = listaCompanieroBd.First();
                                    List<Relacion> listaRelacion = sesionBd.Query<Relacion>().Where(x => x.Individuos.Contains(companiero) && x.Individuos.Contains(rut)).ToList();
                                    Relacion relacion;
                                    if (!listaRelacion.Any())
                                    {
                                        relacion = new Relacion();
                                        relacion.Individuos = new List<string>();
                                        relacion.Individuos.Add(rut);
                                        relacion.Individuos.Add(companiero);
                                        relacion.CantidadRelaciones = 1;
                                        sesionBd.Store(relacion);
                                        // Guardar en BD y generar ID automático para la relación
                                        sesionBd.SaveChanges();
                                        // Guardar la relacion generada en cada individuo
                                        individuo.Relaciones.Add(relacion.Id);
                                        companieroBd.Relaciones.Add(relacion.Id);
                                        // Guardado final de cambios en BD
                                        sesionBd.SaveChanges();
                                    }
                                    else
                                    {
                                        relacion = listaRelacion.First();
                                        relacion.CantidadRelaciones++;
                                        sesionBd.SaveChanges();
                                    }
                                }
                                else
                                {
                                    //evento.Individuos.Remove(companiero);
                                    //sesionBd.SaveChanges();
                                }
                            }
                        }
                        CambiarProgreso(porcentaje);
                    }
                }
                rtbLogResultados.Text += Environment.NewLine + "Lineas leidas: " + numeroLineas;



                // Construir archivos
                List<string> listaRut = new List<string>();
                using (var sesionBd = bd.OpenSession())
                {
                    sesionBd.Advanced.WaitForIndexesAfterSaveChanges();
                    listaRut = sesionBd.Query<Individuo>().Where(x => x.Relaciones.Any()).Select(x => x.Rut).ToList();
                }
                rtbLogResultados.SelectionStart = rtbLogResultados.Text.Length;
                rtbLogResultados.SelectedText = Environment.NewLine;
                int cantidadIndividuos = listaRut.Count;
                lblTituloProgreso.Text = "Creando archivos...";
                CambiarProgreso(0);

                StreamWriter archivoIndividuoIndividuoRelacionSimetrico = new StreamWriter(dialogSeleccionarDirectorioSalida.SelectedPath + "\\IndividuoIndividuoRelacionSimetrico.csv");
                StreamWriter archivoIndividuoIndividuoRelacionNoSimetrico = new StreamWriter(dialogSeleccionarDirectorioSalida.SelectedPath + "\\IndividuoIndividuoRelacionNoSimetrico.csv");
                StreamWriter archivoMatrizEventos = new StreamWriter(dialogSeleccionarDirectorioSalida.SelectedPath + "\\MatrizEventos.csv");

                for (int i = 0; i < cantidadIndividuos; i++)
                {
                    string filaMatrizEventos = "";
                    if (i == 0)
                    {
                        string cabecera = ";" + String.Join(";", listaRut);
                        if (cbMatrizEventos.Checked)
                            archivoMatrizEventos.WriteLine(cabecera);
                    }
                    for (int j = 0; j < cantidadIndividuos; j++)
                    {
                        using (var sesionBd = bd.OpenSession())
                        {
                            sesionBd.Advanced.WaitForIndexesAfterSaveChanges();
                            int maxRequest = sesionBd.Advanced.MaxNumberOfRequestsPerSession;
                            sesionBd.Advanced.MaxNumberOfRequestsPerSession = maxRequest + 30;
                            // Operaciones Matriz eventos
                            if (j == 0)
                                filaMatrizEventos += listaRut[i];
                            if (i == j)
                            {
                                filaMatrizEventos += ";0";
                                continue;
                            }
                            List<Relacion> listaRelaciones = sesionBd.Query<Relacion>().Where(x => x.Individuos.Contains(listaRut[i]) && x.Individuos.Contains(listaRut[j])).ToList();
                            if (listaRelaciones.Any())
                                filaMatrizEventos += ";" + listaRelaciones[0].CantidadRelaciones.ToString();
                            else
                                filaMatrizEventos += ";0";
                            // Operaciones Individuo - Individuo - Relación
                            if (cbIndividuoIndividuoRelacion.Checked && listaRelaciones.Any())
                            {
                                archivoIndividuoIndividuoRelacionSimetrico.WriteLine(listaRut[i] + ";" + listaRut[j] + ";" + listaRelaciones[0].CantidadRelaciones.ToString());
                                if (j > i)
                                    archivoIndividuoIndividuoRelacionNoSimetrico.WriteLine(listaRut[i] + ";" + listaRut[j] + ";" + listaRelaciones[0].CantidadRelaciones.ToString());
                            }
                        }
                    }
                    archivoMatrizEventos.WriteLine(filaMatrizEventos);
                    int porcentaje = ((i + 1) * 100) / cantidadIndividuos;
                    CambiarProgreso(porcentaje);
                }

                archivoIndividuoIndividuoRelacionSimetrico.Close();
                archivoIndividuoIndividuoRelacionNoSimetrico.Close();
                archivoMatrizEventos.Close();

                bd.Maintenance.Server.Send(new DeleteDatabasesOperation("BdPrototipoFiscalia", hardDelete: true, fromNode: null, timeToWaitForConfirmation: null));
            }
            
        }


        private void CambiarProgreso(int porcentaje)
        {
            pbProgreso.Value = porcentaje;
            lblProgreso.Text = porcentaje.ToString() + " %";
        }
    }
}
