﻿namespace PrototipoFiscalia
{
    partial class VentanaFromulario
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelWorkflow = new System.Windows.Forms.Panel();
            this.panelEtapaEntrada = new System.Windows.Forms.Panel();
            this.gbArchivoEntrada = new System.Windows.Forms.GroupBox();
            this.btnBuscarArchivoEntrada = new System.Windows.Forms.Button();
            this.lblNombreArchivoEntrada = new System.Windows.Forms.Label();
            this.gbSeparadorDatos = new System.Windows.Forms.GroupBox();
            this.rbEspacio = new System.Windows.Forms.RadioButton();
            this.rbComa = new System.Windows.Forms.RadioButton();
            this.rbPuntoComa = new System.Windows.Forms.RadioButton();
            this.gbTipoDatos = new System.Windows.Forms.GroupBox();
            this.rbMatrizEventos = new System.Windows.Forms.RadioButton();
            this.rbIndividuoIndividuoRelacion = new System.Windows.Forms.RadioButton();
            this.rbIndividuoEventoFechaFecha = new System.Windows.Forms.RadioButton();
            this.rbIndividuoEvento = new System.Windows.Forms.RadioButton();
            this.lblTituloEtapaEntrada = new System.Windows.Forms.Label();
            this.tlBotonesEtapaEntrada = new System.Windows.Forms.TableLayoutPanel();
            this.btnSiguienteEtapaEntrada = new System.Windows.Forms.Button();
            this.dialogArchivoEntrada = new System.Windows.Forms.OpenFileDialog();
            this.panelEtapaSalida = new System.Windows.Forms.Panel();
            this.gbDirectorioSalida = new System.Windows.Forms.GroupBox();
            this.btnSeleccionDirectorioSalida = new System.Windows.Forms.Button();
            this.lblDirectorioSalida = new System.Windows.Forms.Label();
            this.gbSigma = new System.Windows.Forms.GroupBox();
            this.tbSigma = new System.Windows.Forms.TextBox();
            this.gbArchivosSalida = new System.Windows.Forms.GroupBox();
            this.cbIndicadores = new System.Windows.Forms.CheckBox();
            this.cbMatrizSncse = new System.Windows.Forms.CheckBox();
            this.cbMatrizVinculo = new System.Windows.Forms.CheckBox();
            this.cbMatrizDistancia = new System.Windows.Forms.CheckBox();
            this.cbMatrizEventos = new System.Windows.Forms.CheckBox();
            this.cbIndividuoIndividuoRelacion = new System.Windows.Forms.CheckBox();
            this.lblTituloEtapaSalida = new System.Windows.Forms.Label();
            this.tlBotonesEtapaSalida = new System.Windows.Forms.TableLayoutPanel();
            this.btnSiguienteEtapaSalida = new System.Windows.Forms.Button();
            this.btnAnteriorEtapaSalida = new System.Windows.Forms.Button();
            this.panelEtapaResultados = new System.Windows.Forms.Panel();
            this.lblTituloProgreso = new System.Windows.Forms.Label();
            this.lblProgreso = new System.Windows.Forms.Label();
            this.pbProgreso = new System.Windows.Forms.ProgressBar();
            this.btnGuardarLog = new System.Windows.Forms.Button();
            this.rtbLogResultados = new System.Windows.Forms.RichTextBox();
            this.lblTituloEtapaResultados = new System.Windows.Forms.Label();
            this.tlBotonesEtapaResultado = new System.Windows.Forms.TableLayoutPanel();
            this.btnFinalizarEtapaResultado = new System.Windows.Forms.Button();
            this.btnNuevoEtapaResultado = new System.Windows.Forms.Button();
            this.btnVerResultados = new System.Windows.Forms.Button();
            this.dialogSeleccionarDirectorioSalida = new System.Windows.Forms.FolderBrowserDialog();
            this.bgwOperaciones = new System.ComponentModel.BackgroundWorker();
            this.panelEtapaEntrada.SuspendLayout();
            this.gbArchivoEntrada.SuspendLayout();
            this.gbSeparadorDatos.SuspendLayout();
            this.gbTipoDatos.SuspendLayout();
            this.tlBotonesEtapaEntrada.SuspendLayout();
            this.panelEtapaSalida.SuspendLayout();
            this.gbDirectorioSalida.SuspendLayout();
            this.gbSigma.SuspendLayout();
            this.gbArchivosSalida.SuspendLayout();
            this.tlBotonesEtapaSalida.SuspendLayout();
            this.panelEtapaResultados.SuspendLayout();
            this.tlBotonesEtapaResultado.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelWorkflow
            // 
            this.panelWorkflow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelWorkflow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelWorkflow.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelWorkflow.Location = new System.Drawing.Point(0, 0);
            this.panelWorkflow.Name = "panelWorkflow";
            this.panelWorkflow.Size = new System.Drawing.Size(884, 80);
            this.panelWorkflow.TabIndex = 0;
            // 
            // panelEtapaEntrada
            // 
            this.panelEtapaEntrada.BackColor = System.Drawing.Color.White;
            this.panelEtapaEntrada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEtapaEntrada.Controls.Add(this.gbArchivoEntrada);
            this.panelEtapaEntrada.Controls.Add(this.gbSeparadorDatos);
            this.panelEtapaEntrada.Controls.Add(this.gbTipoDatos);
            this.panelEtapaEntrada.Controls.Add(this.lblTituloEtapaEntrada);
            this.panelEtapaEntrada.Controls.Add(this.tlBotonesEtapaEntrada);
            this.panelEtapaEntrada.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEtapaEntrada.Location = new System.Drawing.Point(0, 80);
            this.panelEtapaEntrada.Name = "panelEtapaEntrada";
            this.panelEtapaEntrada.Size = new System.Drawing.Size(884, 481);
            this.panelEtapaEntrada.TabIndex = 1;
            // 
            // gbArchivoEntrada
            // 
            this.gbArchivoEntrada.Controls.Add(this.btnBuscarArchivoEntrada);
            this.gbArchivoEntrada.Controls.Add(this.lblNombreArchivoEntrada);
            this.gbArchivoEntrada.Location = new System.Drawing.Point(30, 277);
            this.gbArchivoEntrada.Name = "gbArchivoEntrada";
            this.gbArchivoEntrada.Size = new System.Drawing.Size(817, 100);
            this.gbArchivoEntrada.TabIndex = 6;
            this.gbArchivoEntrada.TabStop = false;
            this.gbArchivoEntrada.Text = "Seleccionar archivo de datos";
            // 
            // btnBuscarArchivoEntrada
            // 
            this.btnBuscarArchivoEntrada.Location = new System.Drawing.Point(57, 44);
            this.btnBuscarArchivoEntrada.Name = "btnBuscarArchivoEntrada";
            this.btnBuscarArchivoEntrada.Size = new System.Drawing.Size(143, 27);
            this.btnBuscarArchivoEntrada.TabIndex = 4;
            this.btnBuscarArchivoEntrada.Text = "Seleccionar ...";
            this.btnBuscarArchivoEntrada.UseVisualStyleBackColor = true;
            this.btnBuscarArchivoEntrada.Click += new System.EventHandler(this.btnBuscarArchivoEntrada_Click);
            // 
            // lblNombreArchivoEntrada
            // 
            this.lblNombreArchivoEntrada.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblNombreArchivoEntrada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNombreArchivoEntrada.Location = new System.Drawing.Point(217, 44);
            this.lblNombreArchivoEntrada.Name = "lblNombreArchivoEntrada";
            this.lblNombreArchivoEntrada.Size = new System.Drawing.Size(471, 28);
            this.lblNombreArchivoEntrada.TabIndex = 5;
            // 
            // gbSeparadorDatos
            // 
            this.gbSeparadorDatos.Controls.Add(this.rbEspacio);
            this.gbSeparadorDatos.Controls.Add(this.rbComa);
            this.gbSeparadorDatos.Controls.Add(this.rbPuntoComa);
            this.gbSeparadorDatos.Location = new System.Drawing.Point(467, 72);
            this.gbSeparadorDatos.Name = "gbSeparadorDatos";
            this.gbSeparadorDatos.Size = new System.Drawing.Size(380, 190);
            this.gbSeparadorDatos.TabIndex = 3;
            this.gbSeparadorDatos.TabStop = false;
            this.gbSeparadorDatos.Text = "Separador de datos";
            // 
            // rbEspacio
            // 
            this.rbEspacio.AutoSize = true;
            this.rbEspacio.Location = new System.Drawing.Point(21, 104);
            this.rbEspacio.Name = "rbEspacio";
            this.rbEspacio.Size = new System.Drawing.Size(77, 23);
            this.rbEspacio.TabIndex = 2;
            this.rbEspacio.Text = "Espacio";
            this.rbEspacio.UseVisualStyleBackColor = true;
            // 
            // rbComa
            // 
            this.rbComa.AutoSize = true;
            this.rbComa.Location = new System.Drawing.Point(21, 74);
            this.rbComa.Name = "rbComa";
            this.rbComa.Size = new System.Drawing.Size(90, 23);
            this.rbComa.TabIndex = 1;
            this.rbComa.Text = "Coma ( , )";
            this.rbComa.UseVisualStyleBackColor = true;
            // 
            // rbPuntoComa
            // 
            this.rbPuntoComa.AutoSize = true;
            this.rbPuntoComa.Checked = true;
            this.rbPuntoComa.Location = new System.Drawing.Point(21, 39);
            this.rbPuntoComa.Name = "rbPuntoComa";
            this.rbPuntoComa.Size = new System.Drawing.Size(140, 23);
            this.rbPuntoComa.TabIndex = 0;
            this.rbPuntoComa.TabStop = true;
            this.rbPuntoComa.Text = "Punto y coma ( ; )";
            this.rbPuntoComa.UseVisualStyleBackColor = true;
            // 
            // gbTipoDatos
            // 
            this.gbTipoDatos.Controls.Add(this.rbMatrizEventos);
            this.gbTipoDatos.Controls.Add(this.rbIndividuoIndividuoRelacion);
            this.gbTipoDatos.Controls.Add(this.rbIndividuoEventoFechaFecha);
            this.gbTipoDatos.Controls.Add(this.rbIndividuoEvento);
            this.gbTipoDatos.Location = new System.Drawing.Point(30, 72);
            this.gbTipoDatos.Name = "gbTipoDatos";
            this.gbTipoDatos.Size = new System.Drawing.Size(380, 190);
            this.gbTipoDatos.TabIndex = 2;
            this.gbTipoDatos.TabStop = false;
            this.gbTipoDatos.Text = "Tipo de datos";
            // 
            // rbMatrizEventos
            // 
            this.rbMatrizEventos.AutoSize = true;
            this.rbMatrizEventos.Location = new System.Drawing.Point(22, 129);
            this.rbMatrizEventos.Name = "rbMatrizEventos";
            this.rbMatrizEventos.Size = new System.Drawing.Size(143, 23);
            this.rbMatrizEventos.TabIndex = 3;
            this.rbMatrizEventos.TabStop = true;
            this.rbMatrizEventos.Text = "Matriz de eventos";
            this.rbMatrizEventos.UseVisualStyleBackColor = true;
            // 
            // rbIndividuoIndividuoRelacion
            // 
            this.rbIndividuoIndividuoRelacion.AutoSize = true;
            this.rbIndividuoIndividuoRelacion.Location = new System.Drawing.Point(22, 99);
            this.rbIndividuoIndividuoRelacion.Name = "rbIndividuoIndividuoRelacion";
            this.rbIndividuoIndividuoRelacion.Size = new System.Drawing.Size(227, 23);
            this.rbIndividuoIndividuoRelacion.TabIndex = 2;
            this.rbIndividuoIndividuoRelacion.TabStop = true;
            this.rbIndividuoIndividuoRelacion.Text = "Individuo - Individuo - Relación";
            this.rbIndividuoIndividuoRelacion.UseVisualStyleBackColor = true;
            // 
            // rbIndividuoEventoFechaFecha
            // 
            this.rbIndividuoEventoFechaFecha.AutoSize = true;
            this.rbIndividuoEventoFechaFecha.Location = new System.Drawing.Point(22, 69);
            this.rbIndividuoEventoFechaFecha.Name = "rbIndividuoEventoFechaFecha";
            this.rbIndividuoEventoFechaFecha.Size = new System.Drawing.Size(245, 23);
            this.rbIndividuoEventoFechaFecha.TabIndex = 1;
            this.rbIndividuoEventoFechaFecha.TabStop = true;
            this.rbIndividuoEventoFechaFecha.Text = "Individuo - Evento - Fecha - Fecha";
            this.rbIndividuoEventoFechaFecha.UseVisualStyleBackColor = true;
            // 
            // rbIndividuoEvento
            // 
            this.rbIndividuoEvento.AutoSize = true;
            this.rbIndividuoEvento.Checked = true;
            this.rbIndividuoEvento.Location = new System.Drawing.Point(22, 39);
            this.rbIndividuoEvento.Name = "rbIndividuoEvento";
            this.rbIndividuoEvento.Size = new System.Drawing.Size(143, 23);
            this.rbIndividuoEvento.TabIndex = 0;
            this.rbIndividuoEvento.TabStop = true;
            this.rbIndividuoEvento.Text = "Individuo - Evento";
            this.rbIndividuoEvento.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rbIndividuoEvento.UseVisualStyleBackColor = true;
            // 
            // lblTituloEtapaEntrada
            // 
            this.lblTituloEtapaEntrada.AutoSize = true;
            this.lblTituloEtapaEntrada.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloEtapaEntrada.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblTituloEtapaEntrada.Location = new System.Drawing.Point(11, 12);
            this.lblTituloEtapaEntrada.Name = "lblTituloEtapaEntrada";
            this.lblTituloEtapaEntrada.Size = new System.Drawing.Size(285, 27);
            this.lblTituloEtapaEntrada.TabIndex = 1;
            this.lblTituloEtapaEntrada.Text = "ETAPA DE ENTRADA DE DATOS";
            // 
            // tlBotonesEtapaEntrada
            // 
            this.tlBotonesEtapaEntrada.ColumnCount = 8;
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.420635F));
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.43597F));
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.44081F));
            this.tlBotonesEtapaEntrada.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.419266F));
            this.tlBotonesEtapaEntrada.Controls.Add(this.btnSiguienteEtapaEntrada, 6, 0);
            this.tlBotonesEtapaEntrada.Location = new System.Drawing.Point(0, 407);
            this.tlBotonesEtapaEntrada.Name = "tlBotonesEtapaEntrada";
            this.tlBotonesEtapaEntrada.RowCount = 1;
            this.tlBotonesEtapaEntrada.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlBotonesEtapaEntrada.Size = new System.Drawing.Size(882, 72);
            this.tlBotonesEtapaEntrada.TabIndex = 0;
            // 
            // btnSiguienteEtapaEntrada
            // 
            this.btnSiguienteEtapaEntrada.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSiguienteEtapaEntrada.Location = new System.Drawing.Point(747, 3);
            this.btnSiguienteEtapaEntrada.Name = "btnSiguienteEtapaEntrada";
            this.btnSiguienteEtapaEntrada.Size = new System.Drawing.Size(98, 40);
            this.btnSiguienteEtapaEntrada.TabIndex = 0;
            this.btnSiguienteEtapaEntrada.Text = "Siguiente";
            this.btnSiguienteEtapaEntrada.UseVisualStyleBackColor = true;
            this.btnSiguienteEtapaEntrada.Click += new System.EventHandler(this.btnSiguienteEtapaEntrada_Click);
            // 
            // dialogArchivoEntrada
            // 
            this.dialogArchivoEntrada.Filter = "Archivos (*.txt; *.csv) | *.txt;*.csv";
            // 
            // panelEtapaSalida
            // 
            this.panelEtapaSalida.BackColor = System.Drawing.Color.White;
            this.panelEtapaSalida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEtapaSalida.Controls.Add(this.gbDirectorioSalida);
            this.panelEtapaSalida.Controls.Add(this.gbSigma);
            this.panelEtapaSalida.Controls.Add(this.gbArchivosSalida);
            this.panelEtapaSalida.Controls.Add(this.lblTituloEtapaSalida);
            this.panelEtapaSalida.Controls.Add(this.tlBotonesEtapaSalida);
            this.panelEtapaSalida.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEtapaSalida.Location = new System.Drawing.Point(0, 80);
            this.panelEtapaSalida.Name = "panelEtapaSalida";
            this.panelEtapaSalida.Size = new System.Drawing.Size(884, 481);
            this.panelEtapaSalida.TabIndex = 2;
            this.panelEtapaSalida.Visible = false;
            // 
            // gbDirectorioSalida
            // 
            this.gbDirectorioSalida.Controls.Add(this.btnSeleccionDirectorioSalida);
            this.gbDirectorioSalida.Controls.Add(this.lblDirectorioSalida);
            this.gbDirectorioSalida.Location = new System.Drawing.Point(33, 289);
            this.gbDirectorioSalida.Name = "gbDirectorioSalida";
            this.gbDirectorioSalida.Size = new System.Drawing.Size(814, 92);
            this.gbDirectorioSalida.TabIndex = 8;
            this.gbDirectorioSalida.TabStop = false;
            this.gbDirectorioSalida.Text = "Directorio de almacenamiento de archivos";
            // 
            // btnSeleccionDirectorioSalida
            // 
            this.btnSeleccionDirectorioSalida.Location = new System.Drawing.Point(96, 36);
            this.btnSeleccionDirectorioSalida.Name = "btnSeleccionDirectorioSalida";
            this.btnSeleccionDirectorioSalida.Size = new System.Drawing.Size(143, 27);
            this.btnSeleccionDirectorioSalida.TabIndex = 6;
            this.btnSeleccionDirectorioSalida.Text = "Seleccionar ...";
            this.btnSeleccionDirectorioSalida.UseVisualStyleBackColor = true;
            this.btnSeleccionDirectorioSalida.Click += new System.EventHandler(this.btnSeleccionDirectorioSalida_Click);
            // 
            // lblDirectorioSalida
            // 
            this.lblDirectorioSalida.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDirectorioSalida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDirectorioSalida.Location = new System.Drawing.Point(256, 35);
            this.lblDirectorioSalida.Name = "lblDirectorioSalida";
            this.lblDirectorioSalida.Size = new System.Drawing.Size(471, 28);
            this.lblDirectorioSalida.TabIndex = 7;
            // 
            // gbSigma
            // 
            this.gbSigma.Controls.Add(this.tbSigma);
            this.gbSigma.Location = new System.Drawing.Point(586, 60);
            this.gbSigma.Name = "gbSigma";
            this.gbSigma.Size = new System.Drawing.Size(261, 109);
            this.gbSigma.TabIndex = 3;
            this.gbSigma.TabStop = false;
            this.gbSigma.Text = "Sigma";
            // 
            // tbSigma
            // 
            this.tbSigma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSigma.Enabled = false;
            this.tbSigma.Location = new System.Drawing.Point(60, 47);
            this.tbSigma.Name = "tbSigma";
            this.tbSigma.Size = new System.Drawing.Size(147, 27);
            this.tbSigma.TabIndex = 0;
            this.tbSigma.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbSigma.TextChanged += new System.EventHandler(this.tbSigma_TextChanged);
            this.tbSigma.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSigma_KeyPress);
            // 
            // gbArchivosSalida
            // 
            this.gbArchivosSalida.Controls.Add(this.cbIndicadores);
            this.gbArchivosSalida.Controls.Add(this.cbMatrizSncse);
            this.gbArchivosSalida.Controls.Add(this.cbMatrizVinculo);
            this.gbArchivosSalida.Controls.Add(this.cbMatrizDistancia);
            this.gbArchivosSalida.Controls.Add(this.cbMatrizEventos);
            this.gbArchivosSalida.Controls.Add(this.cbIndividuoIndividuoRelacion);
            this.gbArchivosSalida.Location = new System.Drawing.Point(30, 60);
            this.gbArchivosSalida.Name = "gbArchivosSalida";
            this.gbArchivosSalida.Size = new System.Drawing.Size(509, 223);
            this.gbArchivosSalida.TabIndex = 2;
            this.gbArchivosSalida.TabStop = false;
            this.gbArchivosSalida.Text = "Archivos de salida";
            // 
            // cbIndicadores
            // 
            this.cbIndicadores.AutoSize = true;
            this.cbIndicadores.Location = new System.Drawing.Point(22, 157);
            this.cbIndicadores.Name = "cbIndicadores";
            this.cbIndicadores.Size = new System.Drawing.Size(103, 23);
            this.cbIndicadores.TabIndex = 5;
            this.cbIndicadores.Text = "Indicadores";
            this.cbIndicadores.UseVisualStyleBackColor = true;
            // 
            // cbMatrizSncse
            // 
            this.cbMatrizSncse.AutoSize = true;
            this.cbMatrizSncse.Location = new System.Drawing.Point(22, 131);
            this.cbMatrizSncse.Name = "cbMatrizSncse";
            this.cbMatrizSncse.Size = new System.Drawing.Size(114, 23);
            this.cbMatrizSncse.TabIndex = 4;
            this.cbMatrizSncse.Text = "Matriz SNCSE";
            this.cbMatrizSncse.UseVisualStyleBackColor = true;
            this.cbMatrizSncse.CheckedChanged += new System.EventHandler(this.cbMatrizSncse_CheckedChanged);
            // 
            // cbMatrizVinculo
            // 
            this.cbMatrizVinculo.AutoSize = true;
            this.cbMatrizVinculo.Location = new System.Drawing.Point(22, 105);
            this.cbMatrizVinculo.Name = "cbMatrizVinculo";
            this.cbMatrizVinculo.Size = new System.Drawing.Size(139, 23);
            this.cbMatrizVinculo.TabIndex = 3;
            this.cbMatrizVinculo.Text = "Matriz de vínculo";
            this.cbMatrizVinculo.UseVisualStyleBackColor = true;
            // 
            // cbMatrizDistancia
            // 
            this.cbMatrizDistancia.AutoSize = true;
            this.cbMatrizDistancia.Location = new System.Drawing.Point(22, 78);
            this.cbMatrizDistancia.Name = "cbMatrizDistancia";
            this.cbMatrizDistancia.Size = new System.Drawing.Size(152, 23);
            this.cbMatrizDistancia.TabIndex = 2;
            this.cbMatrizDistancia.Text = "Matriz de distancia";
            this.cbMatrizDistancia.UseVisualStyleBackColor = true;
            // 
            // cbMatrizEventos
            // 
            this.cbMatrizEventos.AutoSize = true;
            this.cbMatrizEventos.Location = new System.Drawing.Point(22, 52);
            this.cbMatrizEventos.Name = "cbMatrizEventos";
            this.cbMatrizEventos.Size = new System.Drawing.Size(144, 23);
            this.cbMatrizEventos.TabIndex = 1;
            this.cbMatrizEventos.Text = "Matriz de eventos";
            this.cbMatrizEventos.UseVisualStyleBackColor = true;
            // 
            // cbIndividuoIndividuoRelacion
            // 
            this.cbIndividuoIndividuoRelacion.AutoSize = true;
            this.cbIndividuoIndividuoRelacion.Location = new System.Drawing.Point(22, 28);
            this.cbIndividuoIndividuoRelacion.Name = "cbIndividuoIndividuoRelacion";
            this.cbIndividuoIndividuoRelacion.Size = new System.Drawing.Size(228, 23);
            this.cbIndividuoIndividuoRelacion.TabIndex = 0;
            this.cbIndividuoIndividuoRelacion.Text = "Individuo - Individuo - Relación";
            this.cbIndividuoIndividuoRelacion.UseVisualStyleBackColor = true;
            // 
            // lblTituloEtapaSalida
            // 
            this.lblTituloEtapaSalida.AutoSize = true;
            this.lblTituloEtapaSalida.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloEtapaSalida.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblTituloEtapaSalida.Location = new System.Drawing.Point(11, 12);
            this.lblTituloEtapaSalida.Name = "lblTituloEtapaSalida";
            this.lblTituloEtapaSalida.Size = new System.Drawing.Size(397, 27);
            this.lblTituloEtapaSalida.TabIndex = 1;
            this.lblTituloEtapaSalida.Text = "ETAPA DE SELECCIÓN DE SALIDA DE DATOS";
            // 
            // tlBotonesEtapaSalida
            // 
            this.tlBotonesEtapaSalida.ColumnCount = 8;
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.420635F));
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.43597F));
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.44081F));
            this.tlBotonesEtapaSalida.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.419266F));
            this.tlBotonesEtapaSalida.Controls.Add(this.btnSiguienteEtapaSalida, 6, 0);
            this.tlBotonesEtapaSalida.Controls.Add(this.btnAnteriorEtapaSalida, 1, 0);
            this.tlBotonesEtapaSalida.Location = new System.Drawing.Point(0, 407);
            this.tlBotonesEtapaSalida.Name = "tlBotonesEtapaSalida";
            this.tlBotonesEtapaSalida.RowCount = 1;
            this.tlBotonesEtapaSalida.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlBotonesEtapaSalida.Size = new System.Drawing.Size(882, 72);
            this.tlBotonesEtapaSalida.TabIndex = 0;
            // 
            // btnSiguienteEtapaSalida
            // 
            this.btnSiguienteEtapaSalida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSiguienteEtapaSalida.Location = new System.Drawing.Point(747, 3);
            this.btnSiguienteEtapaSalida.Name = "btnSiguienteEtapaSalida";
            this.btnSiguienteEtapaSalida.Size = new System.Drawing.Size(98, 40);
            this.btnSiguienteEtapaSalida.TabIndex = 0;
            this.btnSiguienteEtapaSalida.Text = "Siguiente";
            this.btnSiguienteEtapaSalida.UseVisualStyleBackColor = true;
            this.btnSiguienteEtapaSalida.Click += new System.EventHandler(this.btnSiguienteEtapaSalida_Click);
            // 
            // btnAnteriorEtapaSalida
            // 
            this.btnAnteriorEtapaSalida.Location = new System.Drawing.Point(33, 3);
            this.btnAnteriorEtapaSalida.Name = "btnAnteriorEtapaSalida";
            this.btnAnteriorEtapaSalida.Size = new System.Drawing.Size(98, 40);
            this.btnAnteriorEtapaSalida.TabIndex = 1;
            this.btnAnteriorEtapaSalida.Text = "Anterior";
            this.btnAnteriorEtapaSalida.UseVisualStyleBackColor = true;
            this.btnAnteriorEtapaSalida.Click += new System.EventHandler(this.btnAnteriorEtapaSalida_Click);
            // 
            // panelEtapaResultados
            // 
            this.panelEtapaResultados.BackColor = System.Drawing.Color.White;
            this.panelEtapaResultados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEtapaResultados.Controls.Add(this.lblTituloProgreso);
            this.panelEtapaResultados.Controls.Add(this.lblProgreso);
            this.panelEtapaResultados.Controls.Add(this.pbProgreso);
            this.panelEtapaResultados.Controls.Add(this.btnGuardarLog);
            this.panelEtapaResultados.Controls.Add(this.rtbLogResultados);
            this.panelEtapaResultados.Controls.Add(this.lblTituloEtapaResultados);
            this.panelEtapaResultados.Controls.Add(this.tlBotonesEtapaResultado);
            this.panelEtapaResultados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEtapaResultados.Location = new System.Drawing.Point(0, 80);
            this.panelEtapaResultados.Name = "panelEtapaResultados";
            this.panelEtapaResultados.Size = new System.Drawing.Size(884, 481);
            this.panelEtapaResultados.TabIndex = 3;
            this.panelEtapaResultados.Visible = false;
            // 
            // lblTituloProgreso
            // 
            this.lblTituloProgreso.AutoSize = true;
            this.lblTituloProgreso.Location = new System.Drawing.Point(369, 31);
            this.lblTituloProgreso.Name = "lblTituloProgreso";
            this.lblTituloProgreso.Size = new System.Drawing.Size(0, 19);
            this.lblTituloProgreso.TabIndex = 6;
            // 
            // lblProgreso
            // 
            this.lblProgreso.BackColor = System.Drawing.Color.Transparent;
            this.lblProgreso.Location = new System.Drawing.Point(541, 31);
            this.lblProgreso.Name = "lblProgreso";
            this.lblProgreso.Size = new System.Drawing.Size(49, 23);
            this.lblProgreso.TabIndex = 5;
            this.lblProgreso.Text = "0 %";
            this.lblProgreso.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pbProgreso
            // 
            this.pbProgreso.Location = new System.Drawing.Point(596, 26);
            this.pbProgreso.Name = "pbProgreso";
            this.pbProgreso.Size = new System.Drawing.Size(132, 28);
            this.pbProgreso.TabIndex = 4;
            this.pbProgreso.Tag = "";
            // 
            // btnGuardarLog
            // 
            this.btnGuardarLog.Location = new System.Drawing.Point(747, 26);
            this.btnGuardarLog.Name = "btnGuardarLog";
            this.btnGuardarLog.Size = new System.Drawing.Size(98, 28);
            this.btnGuardarLog.TabIndex = 3;
            this.btnGuardarLog.Text = "Guardar Log";
            this.btnGuardarLog.UseVisualStyleBackColor = true;
            // 
            // rtbLogResultados
            // 
            this.rtbLogResultados.Location = new System.Drawing.Point(37, 60);
            this.rtbLogResultados.Name = "rtbLogResultados";
            this.rtbLogResultados.ReadOnly = true;
            this.rtbLogResultados.Size = new System.Drawing.Size(810, 321);
            this.rtbLogResultados.TabIndex = 2;
            this.rtbLogResultados.Text = "";
            // 
            // lblTituloEtapaResultados
            // 
            this.lblTituloEtapaResultados.AutoSize = true;
            this.lblTituloEtapaResultados.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloEtapaResultados.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblTituloEtapaResultados.Location = new System.Drawing.Point(11, 12);
            this.lblTituloEtapaResultados.Name = "lblTituloEtapaResultados";
            this.lblTituloEtapaResultados.Size = new System.Drawing.Size(200, 27);
            this.lblTituloEtapaResultados.TabIndex = 1;
            this.lblTituloEtapaResultados.Text = "LOG DE RESULTADOS";
            // 
            // tlBotonesEtapaResultado
            // 
            this.tlBotonesEtapaResultado.ColumnCount = 8;
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.420635F));
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.43597F));
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.57083F));
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.46032F));
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.43991F));
            this.tlBotonesEtapaResultado.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.419266F));
            this.tlBotonesEtapaResultado.Controls.Add(this.btnFinalizarEtapaResultado, 6, 0);
            this.tlBotonesEtapaResultado.Controls.Add(this.btnNuevoEtapaResultado, 1, 0);
            this.tlBotonesEtapaResultado.Controls.Add(this.btnVerResultados, 5, 0);
            this.tlBotonesEtapaResultado.Location = new System.Drawing.Point(0, 407);
            this.tlBotonesEtapaResultado.Name = "tlBotonesEtapaResultado";
            this.tlBotonesEtapaResultado.RowCount = 1;
            this.tlBotonesEtapaResultado.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlBotonesEtapaResultado.Size = new System.Drawing.Size(882, 72);
            this.tlBotonesEtapaResultado.TabIndex = 0;
            // 
            // btnFinalizarEtapaResultado
            // 
            this.btnFinalizarEtapaResultado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinalizarEtapaResultado.Location = new System.Drawing.Point(747, 3);
            this.btnFinalizarEtapaResultado.Name = "btnFinalizarEtapaResultado";
            this.btnFinalizarEtapaResultado.Size = new System.Drawing.Size(98, 40);
            this.btnFinalizarEtapaResultado.TabIndex = 0;
            this.btnFinalizarEtapaResultado.Text = "Finalizar";
            this.btnFinalizarEtapaResultado.UseVisualStyleBackColor = true;
            this.btnFinalizarEtapaResultado.Click += new System.EventHandler(this.btnFinalizarEtapaResultado_Click);
            // 
            // btnNuevoEtapaResultado
            // 
            this.btnNuevoEtapaResultado.Location = new System.Drawing.Point(33, 3);
            this.btnNuevoEtapaResultado.Name = "btnNuevoEtapaResultado";
            this.btnNuevoEtapaResultado.Size = new System.Drawing.Size(98, 40);
            this.btnNuevoEtapaResultado.TabIndex = 1;
            this.btnNuevoEtapaResultado.Text = "Nuevo";
            this.btnNuevoEtapaResultado.UseVisualStyleBackColor = true;
            this.btnNuevoEtapaResultado.Click += new System.EventHandler(this.btnNuevoEtapaResultado_Click);
            // 
            // btnVerResultados
            // 
            this.btnVerResultados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerResultados.Location = new System.Drawing.Point(586, 3);
            this.btnVerResultados.Name = "btnVerResultados";
            this.btnVerResultados.Size = new System.Drawing.Size(114, 40);
            this.btnVerResultados.TabIndex = 2;
            this.btnVerResultados.Text = "Ver Resultados";
            this.btnVerResultados.UseVisualStyleBackColor = true;
            this.btnVerResultados.Click += new System.EventHandler(this.btnVerResultados_Click);
            // 
            // bgwOperaciones
            // 
            this.bgwOperaciones.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwOperaciones_DoWork);
            // 
            // VentanaFromulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.panelEtapaResultados);
            this.Controls.Add(this.panelEtapaSalida);
            this.Controls.Add(this.panelEtapaEntrada);
            this.Controls.Add(this.panelWorkflow);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "VentanaFromulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prototipo SNCSE";
            this.panelEtapaEntrada.ResumeLayout(false);
            this.panelEtapaEntrada.PerformLayout();
            this.gbArchivoEntrada.ResumeLayout(false);
            this.gbSeparadorDatos.ResumeLayout(false);
            this.gbSeparadorDatos.PerformLayout();
            this.gbTipoDatos.ResumeLayout(false);
            this.gbTipoDatos.PerformLayout();
            this.tlBotonesEtapaEntrada.ResumeLayout(false);
            this.panelEtapaSalida.ResumeLayout(false);
            this.panelEtapaSalida.PerformLayout();
            this.gbDirectorioSalida.ResumeLayout(false);
            this.gbSigma.ResumeLayout(false);
            this.gbSigma.PerformLayout();
            this.gbArchivosSalida.ResumeLayout(false);
            this.gbArchivosSalida.PerformLayout();
            this.tlBotonesEtapaSalida.ResumeLayout(false);
            this.panelEtapaResultados.ResumeLayout(false);
            this.panelEtapaResultados.PerformLayout();
            this.tlBotonesEtapaResultado.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelWorkflow;
        private System.Windows.Forms.Panel panelEtapaEntrada;
        private System.Windows.Forms.Label lblNombreArchivoEntrada;
        private System.Windows.Forms.Button btnBuscarArchivoEntrada;
        private System.Windows.Forms.GroupBox gbSeparadorDatos;
        private System.Windows.Forms.GroupBox gbTipoDatos;
        private System.Windows.Forms.Label lblTituloEtapaEntrada;
        private System.Windows.Forms.TableLayoutPanel tlBotonesEtapaEntrada;
        private System.Windows.Forms.OpenFileDialog dialogArchivoEntrada;
        private System.Windows.Forms.Button btnSiguienteEtapaEntrada;
        private System.Windows.Forms.RadioButton rbMatrizEventos;
        private System.Windows.Forms.RadioButton rbIndividuoIndividuoRelacion;
        private System.Windows.Forms.RadioButton rbIndividuoEventoFechaFecha;
        private System.Windows.Forms.RadioButton rbIndividuoEvento;
        private System.Windows.Forms.RadioButton rbEspacio;
        private System.Windows.Forms.RadioButton rbComa;
        private System.Windows.Forms.RadioButton rbPuntoComa;
        private System.Windows.Forms.Panel panelEtapaSalida;
        private System.Windows.Forms.Label lblTituloEtapaSalida;
        private System.Windows.Forms.TableLayoutPanel tlBotonesEtapaSalida;
        private System.Windows.Forms.Button btnSiguienteEtapaSalida;
        private System.Windows.Forms.Button btnAnteriorEtapaSalida;
        private System.Windows.Forms.GroupBox gbSigma;
        private System.Windows.Forms.TextBox tbSigma;
        private System.Windows.Forms.GroupBox gbArchivosSalida;
        private System.Windows.Forms.CheckBox cbIndicadores;
        private System.Windows.Forms.CheckBox cbMatrizSncse;
        private System.Windows.Forms.CheckBox cbMatrizVinculo;
        private System.Windows.Forms.CheckBox cbMatrizDistancia;
        private System.Windows.Forms.CheckBox cbMatrizEventos;
        private System.Windows.Forms.CheckBox cbIndividuoIndividuoRelacion;
        private System.Windows.Forms.GroupBox gbDirectorioSalida;
        private System.Windows.Forms.Button btnSeleccionDirectorioSalida;
        private System.Windows.Forms.Label lblDirectorioSalida;
        private System.Windows.Forms.GroupBox gbArchivoEntrada;
        private System.Windows.Forms.Panel panelEtapaResultados;
        private System.Windows.Forms.Button btnGuardarLog;
        private System.Windows.Forms.RichTextBox rtbLogResultados;
        private System.Windows.Forms.Label lblTituloEtapaResultados;
        private System.Windows.Forms.TableLayoutPanel tlBotonesEtapaResultado;
        private System.Windows.Forms.Button btnFinalizarEtapaResultado;
        private System.Windows.Forms.Button btnNuevoEtapaResultado;
        private System.Windows.Forms.Button btnVerResultados;
        private System.Windows.Forms.FolderBrowserDialog dialogSeleccionarDirectorioSalida;
        private System.ComponentModel.BackgroundWorker bgwOperaciones;
        private System.Windows.Forms.Label lblProgreso;
        private System.Windows.Forms.ProgressBar pbProgreso;
        private System.Windows.Forms.Label lblTituloProgreso;
    }
}

